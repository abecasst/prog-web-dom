% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

### Calcul d'intérêts composés (niv. 1-2)

D’après w3c get est utilisé pour demander des données à une source spécifique et post est utilisé pour mettre à jour ou créer une ressource sur un serveur. Dans le cas avec notre formulaire la méthode la plus adaptée est le get.


### Un peu de style en CSS (niv. 1)

Les éléments aux niveaux les plus profonds sont les th et les td dans la table. Ils ont une profondeur de 6.


### Calendrier - agenda web (niv. 3)

Pour enregistrer les différents événements ajoutés par l'utilisateur nous avons utilisé les sessions.
On commence notre programme par la fonction session_start qui débute une nouvelle session pour l’utilisateur seulement si une session précédente n’existait pas.
Cette session va permettre de garder en mémoire dans le serveur des données pour l’utilisateur au travers le tableau $_SESSION. 
Nous avons donc ajouté un champ “événements” à $_SESSION qui va contenir d’autres tableaux correspondants aux différents événements enregistrés par l’utilisateur. Ces tableaux contiennent 4 champs : "événement", “year”, “month”, “day”. Grâce ces derniers nous pouvons ajouter à une date donnée les événements correspondants.

Nous avons décidé de procéder de cette manière : lorsque l’utilisateur ajoute un événement, la fonction addEvenement est appelée puis, elle ajoute un événement textuel à une date donnée dans $_SESSION[“evenements”].
Ensuite, lorsqu'on crée le calendrier on appelle la fonction evenementDate pour chaque date qui nous renvoie un tableau contenant tous les événements enregistrés pour une date donnée, on ajoute ensuite ces événements à la date correspondante.

De cette manière on peut avoir plusieurs événements par jour.

Néanmoins comme ce n’était pas demandé nous n’avons pas la possibilité de retirer un événement mais ce serait un ajout intéressant.


## Participants 

* Lucas Tyndal
* Thomas Abecassis



