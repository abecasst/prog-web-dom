<link rel="stylesheet" href="styles.css">

<?php

$lettreCentree=mb_substr($_GET["mot"], 0, 1, 'UTF-8'); //on prend le premier caractère d'un argument
$lettreCentreeEntier = mb_ord(mb_convert_encoding($lettreCentree, "UTF-8", "HTML-ENTITIES")); // On stock sa valeur en entier

print("<table><tr>");
for ($i=$lettreCentreeEntier-6; $i < $lettreCentreeEntier + 10; $i++) { // on itère de manière à centrer sur un caractère
    print("<td>");
    print("<span>" . mb_chr($i) . "</span>");
    print("<br>");
    print("<a href=http://unicode.org/cldr/utility/character.jsp?a=" . dechex($i) . ">U+" . dechex($i) . "</a>");
    print("</td>");
}
print("</tr></table>");
?>
