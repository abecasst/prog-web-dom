<link rel="stylesheet" href="styles.css">
<?php
include_once "form.html";

session_start();//on démare la session
//si le tableau d'evenements n'a pas été créé on le fait
if(!isset($_SESSION["evenements"]))
    $_SESSION["evenements"]=array();

if(isset($_GET["mois"]))
    $month = $_GET["mois"];
else
    $month = 1;

if(isset($_GET["annee"]))
    $year = $_GET["annee"];
else
    $year = 2021;

$evenementPresent=isset($_GET["evenement"]); //on veut savoir si on vient du formulaire d'evenement ou non

if($evenementPresent){ 

    $date = strtotime($_GET['date']);

    $jour=date('d',$date);
    $month=date('m',$date);
    $year=date('Y',$date);

    $evenement = $_GET["evenement"];

    if(strlen($evenement)==0)
        $evenement="evenement vide"; //juste pour l'affichage

    addEvenement($evenement, $year, $month, $jour);
}

$i=1;
$time=mktime(12, 0, 0, $month, $i, $year);
print "<h1>" . date('F', $time) . " " . $year .  "</h1>";

print "<table>";
print "<tr>"; 

while(strcmp(date('D', $time),"Mon")!=0){ // tant que le jour n'est pas lundi on rajoute des cases vides
    $i--;
    $time=mktime(12, 0, 0, $month, $i, $year);
    print "<td></td>";
}


$d=1;
$time=mktime(12, 0, 0, $month, $d, $year);//on met la date au jour souhaité 

while($d<=31 && date('m', $time)==$month)
{ 
    $evenements=evenementDate($year, $month, $d);
    if(strcmp(date('D', $time),"Mon")==0) 
        print "</tr><tr>";
    if($evenements!=NULL){
        print "<td class=\"jourHover\"><span>";
        print "<div class=\"evenement\">";
        foreach ($evenements as $evt) {
            print "<span> - " . $evt .  "</span><br>";
        }
        print "</div>";
    }
    else
        print "<td><span>";

    print date('d', $time); // on affiche le numéro du jour
    print "</span></td>"; 
    $d++;   
    $time=mktime(12, 0, 0, $month, $d, $year);//on met la date au jour souhaité 
}

$d--;//quand on sort de la boucle au dessus on est un jour trop loin donc on doit se replacer à un jour avant
$time=mktime(12, 0, 0, $month, $d, $year);
while(strcmp(date('D', $time),"Sun")!=0){ // tant que le jour n'est pas dimanche on rajoute des cases vides
    $d++;
    $time=mktime(12, 0, 0, $month, $d, $year);
    print "<td></td>";
}

print "</tr>";

print "</table>";

function addEvenement($evenement, $year, $month, $day){ //rajoute un evenement à la session
    array_push($_SESSION["evenements"],   
        array(
            "evenement" => $evenement,
            "year" => $year,
            "month" => $month,
            "day" => $day
        )
    ); //ajoute un tableau à la fin du tableau evenement
}

//retourne un ou plusieurs evenements à une date données NULL si il n'y en a pas
function evenementDate($year, $month, $day){
    $ar=array();
    foreach ($_SESSION["evenements"] as $evenement) {
        if($evenement["year"]==$year && $evenement["month"]==$month && $evenement["day"] == $day){
            array_push($ar, $evenement["evenement"]);
        }
    }
    if(count($ar)==0)
        return NULL;
    return $ar;
}

?>