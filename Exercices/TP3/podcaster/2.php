<link rel="stylesheet" href="styles.css">
<?php
require_once('vendor/dg/rss-php/src/Feed.php');

$rss = Feed::loadRss("http://radiofrance-podcast.net/podcast09/rss_14312.xml");
$tabMethodeScientifique =  creerTableauPHP($rss, "La Méthode Scientifique");


$rss = Feed::loadRss("https://trash-taste-podcast.castos.com/feed");
$tabTrashtaste =  creerTableauPHP($rss, "Trash Taste");

$rss = Feed::loadRss("https://anchor.fm/s/8a18e18/podcast/rss");
$tabFeaturedAnimePodcast =  creerTableauPHP($rss, "FeaturedAnimePodcast");


$tabPodcasts = array_merge($tabMethodeScientifique, $tabTrashtaste);
$tabPodcasts = array_merge($tabPodcasts, $tabFeaturedAnimePodcast);

usort($tabPodcasts, "sortByDate");

creerTableauHTML($tabPodcasts);

function creerTableauPHP($rss, $nomPodcast){
    $podcasts = array();
    foreach ($rss->item as $item) {
        //sélection des podcasts "la méthode scientifique"
        if($nomPodcast!="La Méthode Scientifique" || strpos($item->description, "La Méthode scientifique")!=false){
            $podcast = array();
            $podcast["podcast"]=$nomPodcast;
            $podcast["date"] = $item->pubDate;
            $podcast["titre"] = $item->title;
            $podcast["url"] = $item->enclosure->attributes() ["url"];
            $podcast["mp3"] = $item->enclosure->attributes();
            $podcast["duree"] = $item->{'itunes:duration'};
            array_push($podcasts, $podcast);
        }
    }
    return $podcasts;
}

function sortByDate($a, $b){
    return strtotime($b["date"]) - strtotime($a["date"]);
}

function creerTableauHTML($podcasts){
    $lastWeek = "-1";

    echo "<table>";
    foreach ($podcasts as $podcast) {
    
        $currentWeek = date("W", strtotime($podcast["date"]));
        if(strcmp($lastWeek, $currentWeek) !=0){
            //une nouvelle semaine donc echo ligne intercalaire
            echo '<tr><td colspan = "5">' . $currentWeek . '</td></tr>';
        }
        $lastWeek = $currentWeek;
    
        creerLigneTableau($podcast);
    }
    echo "</table>";
}

function creerLigneTableau($podcast){
    if($podcast["podcast"]=="Trash Taste")
        echo '<tr class="Trash_Taste">';
    else if($podcast["podcast"]=="FeaturedAnimePodcast")
        echo '<tr class="FeaturedAnimePodcast">';
    else
        echo '<tr class="Methode_scientifique">';
    echo '<td> Date: ', $podcast["date"] . "</td>";
	echo '<td> Title: ', $podcast["titre"] . "</td>";
	echo '<td> télecharger :  <a href = "' . $podcast["url"] . '">' . $podcast["url"] . '</a></td>';
    echo '<td> <audio controls="controls"><source src="' . $podcast["mp3"] . '" type = "audio/mp3"/> oups </audio></td>';
    echo '<td> Durée : ' . $podcast["duree"] . "</td></tr>";
    
}

?>