<!-- on veut que toutes les cells aient la meme tailles-->
<style>
table { table-layout: fixed ; width: 100% ;}
td {width: 14.285%;}
</style>

<link rel="stylesheet" href="styles.css">
<?php
require_once('vendor/dg/rss-php/src/Feed.php');


$rss = Feed::loadRss("http://radiofrance-podcast.net/podcast09/rss_14312.xml");
$tabMethodeScientifique =  creerTableauPHP($rss, "La Méthode Scientifique");


$rss = Feed::loadRss("https://trash-taste-podcast.castos.com/feed");
$tabTrashtaste =  creerTableauPHP($rss, "Trash Taste");

$rss = Feed::loadRss("https://anchor.fm/s/8a18e18/podcast/rss");
$tabFeaturedAnimePodcast =  creerTableauPHP($rss, "FeaturedAnimePodcast");


$tabPodcasts = array_merge($tabMethodeScientifique, $tabTrashtaste);
$tabPodcasts = array_merge($tabPodcasts, $tabFeaturedAnimePodcast);
usort($tabPodcasts, "sortByDate");

creerTableauHTML($tabPodcasts);

function sortByDate($a, $b){
    return strtotime($b["date"]) - strtotime($a["date"]);
}

function creerTableauPHP($rss, $nomPodcast){
    $podcasts = array();
    foreach ($rss->item as $item) {
        //sélection des podcasts "la méthode scientifique"
        if($nomPodcast!="La Méthode Scientifique" || strpos($item->description, "La Méthode scientifique")!=false){
            $podcast = array();
            $podcast["podcast"]=$nomPodcast;
            $podcast["date"] = $item->pubDate;
            $podcast["titre"] = $item->title;
            $podcast["url"] = $item->enclosure->attributes() ["url"];
            $podcast["mp3"] = $item->enclosure->attributes();
            $podcast["duree"] = $item->{'itunes:duration'};
            $podcast["link"] = $item->link;
            array_push($podcasts, $podcast);
        }
    }
    return $podcasts;
}

function getTwitterLink($podcast){
    $status = "https://twitter.com/lamethodeFC/status/";
    $page = file_get_contents($podcast["link"]);
    $pos = strpos($page, $status) + strlen($status);
    $link = $status;

    $currentChar = substr($page, $pos, 1);
    while($currentChar != '"'){
        $pos++;
        $link=$link . $currentChar;
        $currentChar = substr($page, $pos, 1);
    }

    return $link;
}

function creerTableauHTML($podcasts){
    $semaines = creerTableauSemaines($podcasts);
    echo '<table border="7">';
    foreach ($semaines as $semaine) {
        echo "<tr>";
        foreach ($semaine as $jour) {
            if($jour==NULL)
                creerCellVide();
            else
                creerPodcastTableau($jour);
        }
        echo "</tr>";
    }
    echo "</tr></table>";
}

function creerTableauSemaines($podcasts){
    $tabSemaines = array();
    $tabSemaine = creerTableauSemaineVide();
    $lastWeek = date("W", strtotime($podcasts[0]["date"])); //pour éviter de débuter en ajoutant une semaine vide dans le tableau
    foreach ($podcasts as $podcast) {

        $time =strtotime($podcast["date"]);
        $currentWeek = date("W", $time);
        $jour = date("l", $time);
        $nouvelleSemaine =strcmp($lastWeek, $currentWeek) !=0;

        if($nouvelleSemaine){
            array_push($tabSemaines, $tabSemaine);
            $tabSemaine=creerTableauSemaineVide();
        }   

        //De cette manière on limite le nombre de podcast par jour à 1
        $tabSemaine[$jour]=$podcast;

        $lastWeek = $currentWeek;
    }
    return $tabSemaines;
}

function creerCellVide(){
    echo '<td colspan="1"></td>';
}


function creerTableauSemaineVide(){
    return array(
        "Monday" =>NULL,
        "Tuesday" =>NULL,
        "Wednesday" =>NULL,
        "Thursday" =>NULL,
        "Friday" =>NULL,
        "Saturday" =>NULL,
        "Sunday" =>NULL
    );
}

function creerPodcastTableau($podcast){
    if($podcast["podcast"]=="Trash Taste")
        echo '<td class="Trash_Taste" colspan="1">', date("F j, Y", strtotime($podcast["date"])) . "<br>";
    else if($podcast["podcast"]=="FeaturedAnimePodcast")
        echo '<td class="FeaturedAnimePodcast" colspan="1">', date("F j, Y", strtotime($podcast["date"])) . "<br>";
    else
        echo '<td class="Methode_scientifique" colspan="1">', date("F j, Y", strtotime($podcast["date"])) . "<br>";
	echo '<a href="' . $podcast["url"] . '"> ', $podcast["titre"] . "</a><br>";
    echo ' <audio ><source src="' . $podcast["mp3"] . '" type = "audio/mp3"/> oups </audio><br>';
    if($podcast["podcast"]=="La Méthode Scientifique"){
        $twitter = getTwitterLink($podcast);
        echo '<a href ="' . $twitter . '"> Twitter</a></td>';
    }
    
}

?>