# Compte-rendu de TP

## Participants 

* Abecassis Thomas
* Thyndal Lucas

### Mise en jambe tmdb
1. Grâce au champ “original_title” on peut voir que le film est Fight Club, on récupère ses informations en format json. En rajoutant le paramètre language=fr on récupère certaines informations en français comme la description du film.

3. On a créé deux fonctions : 
* getArrayMovie($id) qui renvoie un tableau PHP pour un film dont on passe l’id en paramètre. On utilise la fonction tmdbget() pour récupérer le json sous forme de string, ensuite on utilise la fonction json_decode avec comme second paramètre true pour récupérer les information du json sous forme de tableau PHP. Ensuite on forme un nouveau tableau sous la forme “clé” => “valeur” qu’on retourne.
* construitPage($arrayMovie) fait un foreach sur le tableau créé précédemment en affichant la clé puis la valeur.

Le point fort de cette conception est que pour ajouter l’affichage d’un champ en plus il suffit de rajouter une ligne dans la fonction getArrayMovie().

### Mise en jambe podcast

0. Le script a créé des dossiers et fichiers, essentiellement des JSON et des scripts PHP.

1. Notre code fonctionne avec deux fonctions :
* creerTableauHTML() qui crée un tableau PHP à partir du tableau d’objet obtenue à partir de la fonction loadRss.
* creerTableauHTML() qui affiche un tableau HTML à partir du tableau formaté de la fonction précédente.
On relève un premier souci lors de l'implémentation de cette solution. Dans le flux rss il n’y a pas que des podcast de la méthode scientifique, on retrouve aussi “Le Journal des sciences”. On remarque que les podcasts de “La Méthode Scientifique” ont leur description qui commencent systématiquement par “La Méthode Scientifique”. On va utiliser cette propriété pour filtrer les podcasts lors de la création de notre tableauPHP.
Dans la fonction creerTableauPHP() on ajoute un if qui utilise la fonction str_pos sur le champ description des podcasts, de cette manière on peut exclure les podcasts “Le Journal des sciences”.

On a choisi de réaliser le TP des podcasts car nous sommes intéressés par la gestion de flux RSS ainsi que la gestion des fichiers audio.

### TP Podcasts

2. Pour afficher la ligne de séparation à chaque changement de semaine on utilise la fonction date() et deux variables, une contenant le numéro de semaine précedente et une autre contenant le numéro de semaine actuelle dans notre fonction creerTableauHTML(). 
A l’affichage de chaque podcast on appelle la fonction date de la manière suivante : date("W", strtotime($date)) pour récupérer le numéro de la semaine actuelle. Si ce numéro est différent de celui stocké dans la variable semainePrecedente alors on affiche une ligne contenant le numéro de la semaine actuelle.
Cette méthode présente une faille : si il n’y a une an ou plus d’écart entre deux podcasts il est possible de tomber deux fois d'affilée sur le même numéro de semaine et de ne pas afficher de ligne séparatrice alors que les deux podcasts ne sont pas diffusés dans la même semaine. Néanmoins, dans notre cas, cela ne va pas arriver donc notre solution convient.

3. Tout d’abord on relève un problème pour l’affichage du lecteur audio sans contrôle. Sur chromium et firefox lorsqu’on retire la balise controls le lecteur ne s’affiche plus du tout.
Pour l’affichage par semaine, on a modifié le format du tableau PHP. La fonction creerTableauSemaines() renvoie un tableau de tableau. Chaque tableau est d'abord créé par la fonction creerTableauSemaineVide et contient exactement 7 clés, chacunes ayant le nom d’un jour de la semaine. De cette manière pour ajouter un podcast dans le tableau d’une semaine on peut faire 
$tabSemaine[date("l", strtotime($podcast["date"]))]=$podcast. 
Ensuite on a juste à iteré sur le tableau de semaine et afficher une ligne par semaine.

4. Pour cet exercice on a choisi de rajouter deux podcasts, Trash Taste (https://trash-taste-podcast.castos.com/feed) et Featured Anime Podcast (https://anchor.fm/s/8a18e18/podcast/rss).
Comme les flux rss suivent une norme on peut réutiliser nos fonctions précédentes pour ajouter des podcasts dans notre affichage. Pour l’affichage par ordre chronologique on a fusionné les tableaux des trois podcasts en un seul avec la fonction array_merge. Puis on a utilisé la fonction usort sur ce tableau avec la fonction auxilitaire sortByDate($a,$b) qui renvoie un nombre positif si le podcast $b est plus récent que le podcast $a, de cette manière on trie le tableau par ordre du plus récent au plus ancien. On a pas de soucis particulier pour le tableau de la question 2. On a réutilisé les fonctions et ça fonctionne. On a juste rajouté un champ contenant le nom des podcasts pour ajouter des couleurs différenciant les différents podcasts avec une classe css.
Pour le 3 le problème est que le format limite le nombre de podcast à 1 par jour et que les autres podcasts peuvent être diffusés le samedi ou le dimanche. 
On a donc dû étendre l’affichage sur 7 jours et comme l’affichage de podcast est limité à 1 par jour on perd donc des podcasts lorsque deux ou plus sont diffusés le même jour.

5. Le fichier est encodé en stereo et le bitrate est de 128kbps.

6. “lame -m m -b 32 original.mp3 nouveau.mp3”.
Lorsqu’on vérifie les métadonnées on peut voir que le bitrate est bien de 32 kbps et que le fichier est bien en mono. Le fichier est passé de 56 Mo à 14Mo, le gain de stockage n’est pas négligeable cependant lors de l’écoute on constate instantanément la perte de qualité, ce n’est donc pas la solution la plus optimale pour gagner en capacité sur notre mp3, pour améliorer le ratio qualité/poids on peut utiliser une compression VBR qui va faire varier le bitrate en fonction de la "complexité" du morceau à un temps donné (bitrate très bas lors d’un blanc par exemple). 
“lame -m m -v -V 9  original.mp3 nouveau.mp3”. 
Avec le bitrate variable en V9 (configuration la plus basse possible) on obtient un fichier de 18Mo qui est plus agréable à écouter que le précédent, c’est un bon compromis entre qualité et poids. 


7. En analysant les liens twitters on remarque qu’ils débutent tous par “https://twitter.com/lamethodeFC/status/”, on va utiliser cette caractéristique pour analyser la page.
On commence par faire un file_get_input avec le lien obtenu dans l'élément rss pour récupérer la page html, ensuite on cherche la position de la chaine “https://twitter.com/lamethodeFC/status/” dans la page hmtl puis on itère en formant le lien twitter jusqu’à trouver le caractère “.
Cette solution n’est pas optimale pour deux raisons : on fait beaucoup de requêtes pour récupérer les pages html, ce qui est coûteux et lent et de plus, on itère sur de grandes chaînes de caractères.
On pourrait résoudre le second problème en utilisant un parser et utiliser le Xpath ainsi que les règles css pour directement récupérer le lien twitter, ce qui serait plus propre et plus rapide.

8. On peut rajouter le lien dans la frame “User defined URL link”. Néanmoins cela contraint d’utiliser la deuxième version d’id3. 






