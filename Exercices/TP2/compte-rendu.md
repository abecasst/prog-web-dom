% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

## Points d'accès wifi

### 1
On a utilisé la commande "wc -l" qui donne le nombre de ligne du fichier. On obtient 69.

### 2
On a utilisé la commande "sort -u -t, -k2,2 borneswifi_EPSG4326.csv | wc -l". On sort sur la deuxieme colonne seulement les valeurs uniques puis on compte les lignes avec le pipe et wc. On obtient 59 emplacements différents. 

## Antennes GSM

### 1
Il y a 100 antennes. Par rapport aux bornes wifi on a l’opérateur, la technologie et l’adresse de l’antenne.
Cela rend les données plus simples à traiter, par exemple il n'y a pas besoin d’utiliser une api tierce pour trouver l’adresse d'une antenne.


### 2
 Pour ce faire on a utilisé la commande bash "cut -d ";" -f4  DSPE_ANT_GSM_EPSG4326.csv | sort | uniq -c".
 Avec le cut on récupère la quatrième collonne correspondant à l’opérateur, le sort trie les tire par nom et uniq -c regroupe et compte les répetitions succesives de lignes. 

Avec la commande on obtient : 
     26 BYG
     18 FREE
      1 OPERATEUR
     26 ORA
     30 SFR

"OPERATEUR" correspond à la première ligne, il y en a donc 4 différents présents dans le fichier.


### 3
Comme le fichier tient du xml on peut utiliser un validateur xml comme celui proposé par w3school par exemple.

### 4
Le fichier fait approximativement 6500 lignes, soit 65 lignes par antenne. On peut voir beaucoup de répétitions, notamment dans la balise description. Ces deux points rendent le fichier peu lisible.

## Participants 

* Lucas Tyndal
* Thomas Abecassis




