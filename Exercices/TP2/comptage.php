<?php

require_once "../../Helpers/tp2-helpers.php";

$points=creationTab("donnees/borneswifi_EPSG4326.csv");

//print "tout les points : <br>";
//affichePointsDistance($points, INF, 5.72752 ,45.19102);
//print "<br><br><br><br>" ;
//print "tout les points à une distance de moins de 200m : <br>";
//affichePointsDistance($points, 200, 5.72752 ,45.19102);

echo json_encode(getTopN($points, $_GET["lon"] ,$_GET["lat"], $_GET["top"]));



function getTopN($points, $lon, $lat, $N){
    $arr = array();
    $distances = array();
    foreach ($points as $point) {

        $distancePoint = abs(distance(geopoint($point["lon"], $point["lat"]), geopoint($lon, $lat)));
        array_push($distances, $distancePoint);
    }
    
    array_multisort($distances, SORT_ASC, $points);
    sort($distances);
    

    $nbTop = min($N, count($points));
    //print "les " . $nbTop . " points les plus proches : <br>" ;
    for($i =0; $i<$nbTop; $i++){
        array_push($arr, array(
            "nom"=> $points[$i]["name"],
            "adresse" => getAdress($points[$i]),
            "distance" => $distances[$i]
        ));
    }
    return $arr;
}


function getAdress($point){
    $url = "https://api-adresse.data.gouv.fr/reverse/?lon=". $point["lon"] ."&lat=" . $point["lat"]; 
    $json = json_decode(file_get_contents($url), true);
    return $json["features"][0]["properties"]["label"];
}

//affiche les points à une distance inférieur passé en paramètres à la longitude et latitude passées aussi en paramètres
function affichePointsDistance($points, $distance, $lon, $lat){
    $compteur = 0;
    $distanceMini = INF;
    $pointMini;

    foreach ($points as $point) {

        $distancePoint = abs(distance(geopoint($point["lon"], $point["lat"]), geopoint($lon, $lat)));

        if(  $distancePoint < $distance ){
            print $point["name"] . " est à " . $distancePoint . "m <br>";
            $compteur++;
            
            if($distancePoint < $distanceMini){
                $distanceMini = $distancePoint;
                $pointMini = $point;
            }

        }
    }
    if(isset($pointMini))
        print "le point le plus proche est " . $pointMini["name"] . " à " . $distanceMini . "m  <br>" ;
    print "il y a " . $compteur . " points proches  <br>"; 
}

//créer un tableau à partir d'un chemin de fichier
function creationTab($path){
    $csv = file($path);
    //ligne optionnelle si on veut voir le nombre de ligne
    //print "nombre de ligne : " . count($csv);
    
    
    $tableauCsv = array();
    foreach ($csv as $ligne) {
        $tabLigne = str_getcsv($ligne, ",");
        if(is_numeric($tabLigne[3])){
            array_push($tableauCsv, array(
                "name"=>$tabLigne[0],
                "adr"=>$tabLigne[1],
                "lon"=>$tabLigne[2],
                "lat"=>$tabLigne[3]
            )
            );
        }
    }

    return $tableauCsv;
    
}




?>